//package nc.rso.clientapp;
//
//import nc.rso.clientapp.client.entity.Client;
//import nc.rso.clientapp.client.repository.ClientRepository;
//import nc.rso.clientapp.client.service.DefaultClientService;
//import nc.rso.clientapp.client.web.client.ClientManagementWebClient;
//import org.bson.types.ObjectId;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.Optional;
//
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Mockito.*;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class ClientAppApplicationTests {
//
//    private Logger logger = LoggerFactory.getLogger("testLog");
//
//    private Client testClient = new Client("Kulich Svyatoslav Petrovich", "Kabaye", "Minsk",
//            "Svyzistov str. 8-36", "svkulich@gmail.com", "123");
//    private Client testClient2 = new Client("Kulich Svyatoslav Ivanovich", "Kelidon", "Minsk",
//            "Svyzistov str. 8-37", "svkulich@gmail.com", "123456");
//
//    @MockBean
//    private ClientRepository clientRepository;
//
//    @Autowired
//    private DefaultClientService clientService;
//
//    @MockBean
//    private ClientManagementWebClient client;
//
//    @Test
//    public void contextLoads() {
//        logger.warn(String.valueOf(testClient));
//    }
//
//    @Test
//    public void saveClientTest() {
//        when(clientRepository.insert(any(Client.class))).thenReturn(testClient);
//        Client testClientCreated = clientService.save(testClient);
//        assertEquals(testClient, testClientCreated);
//    }
//
//    @Test
//    public void findClientByEmailTest() {
//        when(clientRepository.findByEmail(any(String.class))).thenReturn(Optional.ofNullable(testClient));
//        Client testClientCreated = clientService.findByEmail(testClient.getEmail());
//        assertEquals(testClient, testClientCreated);
//    }
//
//    @Test
//    public void updateClientTest() {
//        testClient.setId(ObjectId.get().toString());
//        testClient2.setId(testClient.getId());
//        when(clientRepository.save(any(Client.class))).thenReturn(testClient2);
//        when(clientRepository.findById(any(String.class))).thenReturn(Optional.of(testClient));
//        Client testClientUpdated = clientService.update(testClient2);
//        assertEquals(testClient2, testClientUpdated);
//    }
//
//    @Test
//    public void deleteClientTest() {
//        doNothing().when(client).deleteAllClientItems(anyString());
//        doNothing().when(clientRepository).deleteByUsername(anyString());
//        when(clientRepository.findByEmail(anyString())).thenReturn(Optional.ofNullable(testClient));
//        clientService.deleteClientByUsername(testClient.getUsername());
//    }
//}
