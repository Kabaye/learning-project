package nc.rso.clientapp.exception;

import org.springframework.http.HttpStatus;

public class RemoteAPIException extends ClientAppException {
    public RemoteAPIException(String message, HttpStatus status) {
        super("Exception from remote API: \"" + message + "\";", status);
    }
}