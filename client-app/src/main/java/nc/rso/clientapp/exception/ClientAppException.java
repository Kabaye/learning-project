package nc.rso.clientapp.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class ClientAppException extends RuntimeException {
    private HttpStatus status;

    ClientAppException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }
}
