package nc.rso.clientapp.exception;

import org.springframework.http.HttpStatus;

public class SecurityException extends ClientAppException {
    public SecurityException(String message) {
        super(message, HttpStatus.UNAUTHORIZED);
    }
}
