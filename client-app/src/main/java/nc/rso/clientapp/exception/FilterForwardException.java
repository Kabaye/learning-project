package nc.rso.clientapp.exception;

import org.springframework.http.HttpStatus;

public class FilterForwardException extends ClientAppException {
    public FilterForwardException(String message, HttpStatus status) {
        super(message, status);
    }
}
