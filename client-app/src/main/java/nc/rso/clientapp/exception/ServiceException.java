package nc.rso.clientapp.exception;

import org.springframework.http.HttpStatus;

public class ServiceException extends ClientAppException {
    public ServiceException(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
