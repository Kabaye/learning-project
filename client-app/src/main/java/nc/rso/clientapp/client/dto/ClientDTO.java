package nc.rso.clientapp.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.StringJoiner;

@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO {
    private String id;
    private String fullName;
    private String username;
    private String city;
    private String address;
    private String email;
    private String password;

    @Override
    public String toString() {
        return new StringJoiner(", ", ClientDTO.class.getSimpleName() + " [", "];")
                .add("id = '" + id + "'")
                .add("fullName = '" + fullName + "'")
                .add("username = '" + username + "'")
                .add("city = '" + city + "'")
                .add("address = '" + address + "'")
                .add("email = '" + email + "'")
                .add("password = [Protected]")
                .toString();
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
