package nc.rso.clientapp.client.validation;

import java.util.regex.Pattern;

public interface EmailValidator extends PropertyValidator {
    Pattern getEmailPattern();
}
