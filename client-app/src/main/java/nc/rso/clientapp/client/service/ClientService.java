package nc.rso.clientapp.client.service;

import nc.rso.clientapp.client.entity.Client;

import java.util.Collection;

public interface ClientService {
    Client save(Client client);

    Client findByEmail(String email);

    Client findByUsername(String username);

    Client update(Client client);

    void deleteClientByUsername(String email);

    Collection<Client> findAll();
}
