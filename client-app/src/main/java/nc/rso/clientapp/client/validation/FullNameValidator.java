package nc.rso.clientapp.client.validation;

public interface FullNameValidator extends PropertyValidator {
    Integer getMinNameLength();
}
