package nc.rso.clientapp.client.validation;

import lombok.Getter;
import nc.rso.clientapp.exception.ServiceException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Validator
@Getter
public class ClientEmailValidator implements EmailValidator {
    private final Pattern emailPattern;

    public ClientEmailValidator(Pattern emailPattern) {
        this.emailPattern = emailPattern;
    }

    @Override
    public void check(String email) {
        if (email != null) {
            Matcher matcher = emailPattern.matcher(email);
            if (!matcher.matches()) {
                throw new ServiceException("E-mail \"" + email + "\" is invalid.");
            }
        } else {
            throw new ServiceException("E-mail must be not null.");
        }
    }
}
