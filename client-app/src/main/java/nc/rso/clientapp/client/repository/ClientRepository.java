package nc.rso.clientapp.client.repository;

import nc.rso.clientapp.client.entity.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends MongoRepository<Client, String> {

    Optional<Client> findByEmail(String email);

    void deleteByUsername(String username);

    Optional<Client> findByUsername(String username);
}
