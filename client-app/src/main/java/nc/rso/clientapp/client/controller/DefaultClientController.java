package nc.rso.clientapp.client.controller;

import nc.rso.clientapp.client.dto.ClientDTO;
import nc.rso.clientapp.client.entity.Client;
import nc.rso.clientapp.client.mapper.ClientMapper;
import nc.rso.clientapp.client.service.ClientService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/clients")
public class DefaultClientController {
    private final ClientService clientService;
    private final ClientMapper mapper;

    public DefaultClientController(ClientService clientService, ClientMapper mapper) {
        this.clientService = clientService;
        this.mapper = mapper;
    }

    @GetMapping(value = "/email/{email}")
    public ClientDTO findByEmail(@PathVariable String email) {
        return mapper.toDto(clientService.findByEmail(email));
    }

    @GetMapping(value = "/username/{username}")
    public Client findByUsername(@PathVariable String username) {
        return clientService.findByUsername(username);
    }

    @GetMapping
    public Collection<ClientDTO> findAll() {
        return clientService.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ClientDTO update(@PathVariable String id, @RequestBody ClientDTO clientDTO) {
        Client client = mapper.toEntity(clientDTO);
        client.setId(id);
        return mapper.toDto(clientService.update(client));
    }

    @DeleteMapping(value = "/{username}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String username) {
        clientService.deleteClientByUsername(username);
    }
}
