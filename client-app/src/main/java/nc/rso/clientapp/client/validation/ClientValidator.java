package nc.rso.clientapp.client.validation;

import lombok.Getter;
import nc.rso.clientapp.client.entity.Client;
import nc.rso.clientapp.exception.ServiceException;

import java.util.Objects;
import java.util.function.Consumer;

@Validator
@Getter
public class ClientValidator implements ServiceValidator<Client, String> {
    private final PropertyValidator emailValidator;
    private final PropertyValidator fullNameValidator;

    public ClientValidator(EmailValidator emailValidator, FullNameValidator fullNameValidator) {
        this.emailValidator = emailValidator;
        this.fullNameValidator = fullNameValidator;
    }

    @Override
    public String extractId(Client resource) {
        return resource.getId();
    }

    @Override
    public void checkIdIsNull(String id) {
        if (Objects.nonNull(id)) {
            throw new ServiceException("ID of client must be null.");
        }
    }

    @Override
    public void checkIdIsNotNull(String id) {
        if (Objects.isNull(id)) {
            throw new ServiceException("ID of client must be not null.");
        }
    }

    @Override
    public void checkNotNull(Client resource) {
        if (Objects.isNull(resource)) {
            throw new ServiceException("Client can't be null.");
        }
    }

    @Override
    public void checkNotNull(Client resource, String errorMessage) {
        if (Objects.isNull(resource)) {
            throw new ServiceException(errorMessage);
        }
    }

    @Override
    public void checkProperties(Client resource) {
        emailValidator.check(resource.getEmail());
        fullNameValidator.check(resource.getFullName());
    }

    @Override
    public <P> void checkProperty(P property, Consumer<P> checker) {
        checker.accept(property);
    }
}
