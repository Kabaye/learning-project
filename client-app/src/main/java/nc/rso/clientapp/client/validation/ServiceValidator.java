package nc.rso.clientapp.client.validation;

import java.util.function.Consumer;

public interface ServiceValidator<T, I> {
    I extractId(T resource);

    void checkIdIsNull(I id);

    void checkIdIsNotNull(I id);

    void checkNotNull(T resource);

    void checkNotNull(T resource, String errorMessage);

    default <V> void checkFoundByProperty(T resource, String property, V value) {
        checkNotNull(resource, "Resource with " + property + " = \"" + value + "\" doesn't exist.");
    }

    void checkProperties(T resource);

    default void checkForSave(T resource) {
        checkNotNull(resource);
        checkIdIsNull(extractId(resource));
        checkProperties(resource);
    }

    default void checkForUpdate(T resource) {
        checkNotNull(resource);
        checkIdIsNotNull(extractId(resource));
        checkProperties(resource);
    }

    <P> void checkProperty(P property, Consumer<P> checker);
}
