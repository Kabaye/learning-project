package nc.rso.clientapp.client.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;
import java.util.StringJoiner;

@Document(collection = "clients")
@Setter
@Getter
@NoArgsConstructor
public class Client {

    @Id
    private String id;
    private String fullName;
    @Indexed(unique = true)
    private String username;
    private String city;
    private String address;
    @Indexed(unique = true)
    private String email;
    private String password;

    public Client(String fullName, String username, String city, String address, String email, String password) {
        this.fullName = fullName;
        this.username = username;
        this.city = city;
        this.address = address;
        this.email = email;
        this.password = password;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Client.class.getSimpleName() + " [", "];")
                .add("id = '" + id + "'")
                .add("fullName = '" + fullName + "'")
                .add("username = '" + username + "'")
                .add("city = '" + city + "'")
                .add("address = '" + address + "'")
                .add("email = '" + email + "'")
                .add("password = [Protected]")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return Objects.equals(id, client.id) &&
                Objects.equals(fullName, client.fullName) &&
                Objects.equals(username, client.username) &&
                Objects.equals(city, client.city) &&
                Objects.equals(address, client.address) &&
                Objects.equals(email, client.email) &&
                Objects.equals(password, client.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, username, city, address, email, password);
    }
}
