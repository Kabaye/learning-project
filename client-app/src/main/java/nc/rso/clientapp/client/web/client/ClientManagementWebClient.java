package nc.rso.clientapp.client.web.client;

import nc.rso.clientapp.security.util.JwtTokenUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ClientManagementWebClient {
    private static final String ITEM_APP_REQUEST_PARAM = "client-username";
    private final RestTemplate restTemplate;
    private final String orderManagementUrl;
    private final JwtTokenUtils jwtTokenUtils;
    private final UserDetailsService defaultUserDetailsService;
    private final HttpHeaders headers = new HttpHeaders();

    public ClientManagementWebClient(RestTemplate restTemplate, String orderManagementUrl, JwtTokenUtils jwtTokenUtils, UserDetailsService defaultUserDetailsService) {
        this.restTemplate = restTemplate;
        this.orderManagementUrl = orderManagementUrl;
        this.jwtTokenUtils = jwtTokenUtils;
        this.defaultUserDetailsService = defaultUserDetailsService;
    }

    public void deleteAllClientItems(String clientUsername) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(orderManagementUrl);
        uriComponentsBuilder.queryParam(ITEM_APP_REQUEST_PARAM, clientUsername);
        headers.setBearerAuth(jwtTokenUtils.generateToken(defaultUserDetailsService.loadUserByUsername(clientUsername)));
        restTemplate.exchange(uriComponentsBuilder.build().toUriString(), HttpMethod.DELETE, new HttpEntity<>(headers), void.class);
    }
}
