package nc.rso.clientapp.client.validation;

public interface PropertyValidator {
    void check(String property);
}
