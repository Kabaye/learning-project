package nc.rso.clientapp.client.service;

import nc.rso.clientapp.client.entity.Client;
import nc.rso.clientapp.client.repository.ClientRepository;
import nc.rso.clientapp.client.validation.ClientValidator;
import nc.rso.clientapp.client.validation.ServiceValidator;
import nc.rso.clientapp.client.web.client.ClientManagementWebClient;
import nc.rso.clientapp.exception.EntityNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Objects;

@Transactional
@Service
public class DefaultClientService implements ClientService {
    private final ServiceValidator<Client, String> clientValidator;
    private final ClientRepository clientRepository;
    private final ClientManagementWebClient clientManagementWebClient;
    private final PasswordEncoder passwordEncoder;

    public DefaultClientService(ServiceValidator<Client, String> clientValidator, ClientRepository clientRepository,
                                ClientManagementWebClient clientManagementWebClient, PasswordEncoder passwordEncoder) {
        this.clientValidator = clientValidator;
        this.clientRepository = clientRepository;
        this.clientManagementWebClient = clientManagementWebClient;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Client save(Client client) {
        clientValidator.checkForSave(client);
        client.setPassword(passwordEncoder.encode(client.getPassword()));
        return clientRepository.insert(client);
    }

    @Override
    public Client findByEmail(final String email) {
        clientValidator.checkProperty(email, ((ClientValidator) clientValidator).getEmailValidator()::check);
        return clientRepository.findByEmail(email).orElseThrow(() -> new EntityNotFoundException("Client with e-mail: \'" + email + "\' doesn't exist."));
    }

    @Override
    public Client findByUsername(String username) {
        return clientRepository.findByUsername(username).orElseThrow(() -> new EntityNotFoundException("Client with username: \'" + username + "\' doesn't exist."));
    }

    @Override
    public Client update(Client client) {
        clientValidator.checkForUpdate(client);
        Client dbClient = clientRepository.findById(client.getId()).orElseThrow(() -> new EntityNotFoundException("Client with id: \'" + client.getId() + "\' doesn't exist."));

        if (Objects.isNull(client.getEmail())) {
            client.setEmail(dbClient.getEmail());
        }
        if (Objects.isNull(client.getUsername())) {
            client.setUsername(dbClient.getUsername());
        }
        if (Objects.isNull(client.getPassword())) {
            client.setPassword(dbClient.getPassword());
        } else {
            client.setPassword(passwordEncoder.encode(client.getPassword()));
        }

        return clientRepository.save(client);
    }

    @Override
    public void deleteClientByUsername(String username) {
        findByUsername(username);
        clientManagementWebClient.deleteAllClientItems(username);
        clientRepository.deleteByUsername(username);
    }

    @Override
    public Collection<Client> findAll() {
        return clientRepository.findAll();
    }
}
