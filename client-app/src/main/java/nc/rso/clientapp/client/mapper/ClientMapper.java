package nc.rso.clientapp.client.mapper;

import nc.rso.clientapp.client.dto.ClientDTO;
import nc.rso.clientapp.client.entity.Client;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ClientMapper {
    private final ModelMapper modelMapper;

    public ClientMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Client toEntity(ClientDTO dto) {
        return Objects.isNull(dto) ? null : modelMapper.map(dto, Client.class);
    }

    public ClientDTO toDto(Client entity) {
        return Objects.isNull(entity) ? null : modelMapper.map(entity, ClientDTO.class);
    }
}
