package nc.rso.clientapp.client.validation;

import lombok.Getter;
import nc.rso.clientapp.exception.ServiceException;

@Getter
@Validator
public class ClientFullNameValidator implements FullNameValidator {
    private final Integer minNameLength;

    public ClientFullNameValidator(Integer minNameLength) {
        this.minNameLength = minNameLength;
    }

    @Override
    public void check(String fullName) {
        if (fullName == null || fullName.trim().replaceAll(" +", " ").length() < getMinNameLength()) {
            throw new ServiceException("FIO \"" + fullName + "\" does not contain enough letters.");
        }
    }
}
