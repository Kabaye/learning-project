package nc.rso.clientapp.configuration.security;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.KeyPair;

@Configuration
public class SecurityConstantConfiguration {

    @Bean
    public Integer jwtTokenValidity(@Value("${jwt.token.validity}") Integer validity) {
        return validity;
    }

    @Bean
    public String tokenType(@Value("${jwt.token.type}") String tokenType) {
        return tokenType;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(5);
    }

    @Bean
    public KeyPair keyPair() {
        return Keys.keyPairFor(SignatureAlgorithm.RS512);
    }
}
