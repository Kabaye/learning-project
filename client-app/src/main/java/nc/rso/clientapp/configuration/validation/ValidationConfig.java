package nc.rso.clientapp.configuration.validation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.regex.Pattern;


@Configuration
public class ValidationConfig {
    private static final Integer DEFAULT_MIN_FIO_LENGTH = 5;

    private static final Integer MIN_USERNAME_LENGTH = 3;

    @Bean
    public Integer minNameLength() {
        return DEFAULT_MIN_FIO_LENGTH;
    }

    @Bean
    public Pattern emailPattern() {
        return Pattern.compile("([a-z0-9]([._\\-]?[a-z0-9]+)*){" + MIN_USERNAME_LENGTH + "}[@][a-z][a-z]*[.]((org)|(net)|(ru)|(com)|(by))", Pattern.CASE_INSENSITIVE);
    }
}
