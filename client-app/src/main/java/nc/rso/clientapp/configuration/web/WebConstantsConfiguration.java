package nc.rso.clientapp.configuration.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConstantsConfiguration {
    @Bean
    public String orderManagementUrl(@Value("${order-management.url}") String orderManagementUrl) {
        return orderManagementUrl;
    }

    @Bean
    public String clientManagementUrl(@Value("${client-management.url}") String clientManagementUrl) {
        return clientManagementUrl;
    }
}
