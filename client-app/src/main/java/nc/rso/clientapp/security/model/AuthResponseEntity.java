package nc.rso.clientapp.security.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class AuthResponseEntity implements Serializable {

    private static final long serialVersionUID = 6138619177565497396L;

    private String jwtToken;
    private String tokenType;
    private Long expiresIn;
}
