package nc.rso.clientapp.security.service;

import nc.rso.clientapp.client.entity.Client;
import nc.rso.clientapp.client.repository.ClientRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class DefaultUserDetailsService implements UserDetailsService {
    private final ClientRepository clientRepository;

    public DefaultUserDetailsService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = clientRepository.findByUsername(username).orElseThrow(() -> new SecurityException("Username or password are not correct."));
        return new User(client.getUsername(), client.getPassword(), new ArrayList<>());
    }
}
