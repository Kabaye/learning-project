package nc.rso.clientapp.security.controller;

import nc.rso.clientapp.client.dto.ClientDTO;
import nc.rso.clientapp.client.mapper.ClientMapper;
import nc.rso.clientapp.client.service.DefaultClientService;
import nc.rso.clientapp.security.model.AuthRequestEntity;
import nc.rso.clientapp.security.model.AuthResponseEntity;
import nc.rso.clientapp.security.util.JwtTokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtils tokenUtil;
    private final DefaultClientService clientService;
    private final UserDetailsService defaultUserDetailsService;
    private final ClientMapper mapper;
    private final String tokenType;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, JwtTokenUtils tokenUtil, DefaultClientService clientService,
                          UserDetailsService defaultUserDetailsService, ClientMapper mapper, String tokenType) {
        this.authenticationManager = authenticationManager;
        this.tokenUtil = tokenUtil;
        this.clientService = clientService;
        this.defaultUserDetailsService = defaultUserDetailsService;
        this.mapper = mapper;
        this.tokenType = tokenType;
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ClientDTO save(@RequestBody ClientDTO clientDTO) {
        return mapper.toDto(clientService.save(mapper.toEntity(clientDTO)));
    }

    @PostMapping("/authenticate")
    @ResponseStatus(HttpStatus.OK)
    public AuthResponseEntity authenticate(@RequestBody AuthRequestEntity request) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        } catch (AuthenticationException e) {
            throw new SecurityException(e.getMessage());
        }
        final UserDetails userDetails = defaultUserDetailsService.loadUserByUsername(request.getUsername());
        final String token = tokenUtil.generateToken(userDetails);
        return new AuthResponseEntity(token, tokenType, tokenUtil.getExpirationTimeInSeconds(token));
    }

    @PostMapping("/public-key")
    @ResponseStatus(HttpStatus.OK)
    public byte[] getPublicKey() {
        return tokenUtil.getPublicKey().getEncoded();
    }
}
