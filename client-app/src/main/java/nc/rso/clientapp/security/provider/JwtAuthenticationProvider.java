package nc.rso.clientapp.security.provider;

import nc.rso.clientapp.security.authentication.JwtTokenAuthentication;
import nc.rso.clientapp.security.util.JwtTokenUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {
    private final JwtTokenUtils jwtTokenUtils;

    public JwtAuthenticationProvider(JwtTokenUtils jwtTokenUtils) {
        this.jwtTokenUtils = jwtTokenUtils;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (authentication instanceof JwtTokenAuthentication) {
            JwtTokenAuthentication jwtTokenAuthentication = (JwtTokenAuthentication) authentication;
            jwtTokenUtils.validateTokenAndGetUsername(jwtTokenAuthentication.getToken());
            authentication.setAuthenticated(true);
            return authentication;
        }
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtTokenAuthentication.class.isAssignableFrom(authentication);
    }
}
