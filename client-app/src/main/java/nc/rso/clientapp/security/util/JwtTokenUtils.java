package nc.rso.clientapp.security.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtils implements Serializable {
    private static final long serialVersionUID = -4858683069632423656L;
    private final Integer tokenValidity;
    private final KeyPair keyPair;
    private final String clientManagementUrl;

    @Autowired
    public JwtTokenUtils(Integer jwtTokenValidity, KeyPair keyPair, String clientManagementUrl) {
        this.tokenValidity = jwtTokenValidity;
        this.keyPair = keyPair;
        this.clientManagementUrl = clientManagementUrl;
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(keyPair.getPublic()).parseClaimsJws(token).getBody();
    }

    private <T> T getDataFromClaims(Claims claims, Function<Claims, T> claimsResolver) {
        return claimsResolver.apply(claims);
    }

    public String validateTokenAndGetUsername(String token) {
        Claims claims = getAllClaimsFromToken(token);
        return getDataFromClaims(claims, Claims::getSubject);
    }

    public Long getExpirationTimeInSeconds(String token) {
        Claims claims = getAllClaimsFromToken(token);
        Date date = getDataFromClaims(claims, Claims::getExpiration);
        return (date.getTime() - System.currentTimeMillis()) / 1000;
    }

    //generate token for user
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + tokenValidity * 1000))
                .setSubject(subject)
                .setIssuer(clientManagementUrl)
                .signWith(keyPair.getPrivate()).compact();

    }

    public PublicKey getPublicKey() {
        return keyPair.getPublic();
    }
}
