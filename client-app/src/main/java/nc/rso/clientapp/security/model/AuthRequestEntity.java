package nc.rso.clientapp.security.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AuthRequestEntity implements Serializable {
    private static final long serialVersionUID = -6945865496905227793L;

    private String username;
    private String password;
}
