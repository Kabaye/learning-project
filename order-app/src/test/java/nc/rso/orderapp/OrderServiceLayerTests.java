//package nc.rso.orderapp;
//
//import nc.rso.orderapp.configuration.project.ProjectConfiguration;
//import nc.rso.orderapp.exception.ServiceException;
//import nc.rso.orderapp.order.dao.OrderDAO;
//import nc.rso.orderapp.order.entity.Order;
//import nc.rso.orderapp.order.service.DefaultOrderService;
//import nc.rso.orderapp.order.service.OrderService;
//import nc.rso.orderapp.order.validation.ServiceValidator;
//import org.bson.types.ObjectId;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Mockito.*;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = ProjectConfiguration.class)
//public class OrderServiceLayerTests {
//    @Autowired
//    private ServiceValidator<Order, ObjectId> serviceValidator;
//
//    private OrderService orderService;
//    private OrderDAO orderDAO;
//
//    private Order testOrder1;
//    private Order testOrder2;
//    private Order testOrder3;
//
//
//    @Before
//    public void init() {
//        orderDAO = mock(OrderDAO.class);
//
//        orderService = new DefaultOrderService(orderDAO, serviceValidator);
//
//        testOrder1 = new Order(null, "svkulich@gmail.com", "Lambo", "Lambo limited", "Italia", 9D, "штуки");
//        testOrder2 = new Order(ObjectId.get(), "pkulich@gmail.com", "Chocolate", "Nestle", "Ukraine", 100D, "кг");
//        testOrder3 = new Order(ObjectId.get(), "pkulich@gmail.com", "Cho", "Nestle", "Ukraine", 100D, "кг");
//    }
//
//    @Test
//    public void saveOrderTest() {
//        when(orderDAO.save(any(Order.class))).thenReturn(testOrder1);
//        Order actualOrder = orderService.save(testOrder1);
//        assertEquals(testOrder1, actualOrder);
//    }
//
//    @Test
//    public void updateOrderTest() {
//        when(orderDAO.update(any(Order.class))).thenReturn(testOrder2);
//        Order actualOrder = orderService.update(testOrder2);
//        assertEquals(testOrder2, actualOrder);
//    }
//
//    @Test
//    public void deleteTest() {
//        doNothing().when(orderDAO).delete(any(ObjectId.class));
//        orderService.delete(testOrder2.getId().toString());
//    }
//
//    @Test(expected = ServiceException.class)
//    public void updateTest2() {
//        when(orderDAO.update(any(Order.class))).thenReturn(testOrder2);
//        orderService.update(testOrder3);
//    }
//}
