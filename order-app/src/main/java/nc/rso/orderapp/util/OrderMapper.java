package nc.rso.orderapp.util;

import nc.rso.orderapp.order.dto.OrderDTO;
import nc.rso.orderapp.order.entity.Order;
import org.bson.types.ObjectId;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class OrderMapper {
    private final ModelMapper modelMapper;

    public OrderMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        Converter<String, ObjectId> idConverter = s -> Objects.nonNull(s.getSource()) ? new ObjectId(s.getSource()) : null;
        PropertyMap<OrderDTO, Order> goodMapping = new PropertyMap<>() {
            protected void configure() {
                using(idConverter).map(source.getId(), destination.getId());
            }
        };
        modelMapper.addMappings(goodMapping);
    }

    public Order toEntity(OrderDTO dto) {
        return Objects.isNull(dto) ? null : modelMapper.map(dto, Order.class);
    }

    public OrderDTO toDto(Order entity) {
        return Objects.isNull(entity) ? null : modelMapper.map(entity, OrderDTO.class);
    }
}
