package nc.rso.orderapp.security.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Objects;
import java.util.function.Function;

@Component
public class JwtTokenUtils implements Serializable {
    private static final long serialVersionUID = -1565968882724704329L;
    private final RestTemplate restTemplate;
    private final String clientManagementBaseUrl;
    private final String getPublicKeyEndpoint;

    public JwtTokenUtils(RestTemplate restTemplate, String clientManagementBaseUrl, String getPublicKeyEndpoint) {
        this.restTemplate = restTemplate;
        this.clientManagementBaseUrl = clientManagementBaseUrl;
        this.getPublicKeyEndpoint = getPublicKeyEndpoint;
    }

    private Claims getAllClaimsFromToken(String token) {
        byte[] publicKeyBytes = restTemplate.postForObject(clientManagementBaseUrl + getPublicKeyEndpoint, null, byte[].class);
        if (Objects.isNull(publicKeyBytes)) {
            throw new SecurityException("Empty Public key. Something go wrong.");
        }

        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
        PublicKey publicKey;
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(publicKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new SecurityException(e.getMessage());
        }
        return Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
    }

    private <T> T getDataFromClaims(Claims claims, Function<Claims, T> claimsResolver) {
        return claimsResolver.apply(claims);
    }

    public String validateTokenAndGetUsername(String token) {
        Claims claims = getAllClaimsFromToken(token);
        return getDataFromClaims(claims, Claims::getSubject);
    }
}
