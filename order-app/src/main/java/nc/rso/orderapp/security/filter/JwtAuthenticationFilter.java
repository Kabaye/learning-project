package nc.rso.orderapp.security.filter;

import nc.rso.orderapp.exception.FilterForwardException;
import nc.rso.orderapp.security.authentication.JwtTokenAuthentication;
import nc.rso.orderapp.security.util.JwtTokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private final JwtTokenUtils jwtTokenUtils;
    private final String tokenType;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public JwtAuthenticationFilter(JwtTokenUtils jwtTokenUtils, String tokenType, AuthenticationManager authenticationManager) {
        this.jwtTokenUtils = jwtTokenUtils;
        this.tokenType = tokenType + " ";
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String requestTokenHeader = request.getHeader(AUTHORIZATION);

        String username, jwtToken;
        if (Objects.nonNull(requestTokenHeader) && requestTokenHeader.startsWith(tokenType)) {
            jwtToken = requestTokenHeader.substring(tokenType.length());
            username = jwtTokenUtils.validateTokenAndGetUsername(jwtToken);

            try {
                Authentication authentication = authenticationManager.authenticate(
                        new JwtTokenAuthentication(jwtToken, username));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (Exception ex) {
                throw new FilterForwardException(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        filterChain.doFilter(request, response);
    }
}
