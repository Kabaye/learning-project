package nc.rso.orderapp.security.authentication;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;

public class JwtTokenAuthentication extends AbstractAuthenticationToken {
    private String token;
    private String username;

    public JwtTokenAuthentication(String token, String username) {
        super(AuthorityUtils.NO_AUTHORITIES);
        this.token = token;
        this.username = username;
    }

    @Override
    public Object getCredentials() {
        return "N/A";
    }

    @Override
    public Object getPrincipal() {
        return username;
    }

    public String getToken() {
        return token;
    }
}
