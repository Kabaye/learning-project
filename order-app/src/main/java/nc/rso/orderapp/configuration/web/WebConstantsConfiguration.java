package nc.rso.orderapp.configuration.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConstantsConfiguration {
    @Bean
    public String servletContextPath(@Value("${server.servlet.context-path}") String contextPath) {
        return contextPath;
    }

    @Bean
    public String getClientByUsernameEndpoint(@Value("${client-management.app.endpoint.for-get-client-by-username}") String getClientByUsernameEndpoint) {
        return getClientByUsernameEndpoint;
    }

    @Bean
    public String clientManagementBaseUrl(@Value("${client-management.base-url}") String clientManagementUrl) {
        return clientManagementUrl;
    }

    @Bean
    public String clientControllerUrl(@Value("${client-management.client-controller.url}") String clientControllerUrl) {
        return clientControllerUrl;
    }
}
