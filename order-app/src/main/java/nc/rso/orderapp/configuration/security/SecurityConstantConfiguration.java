package nc.rso.orderapp.configuration.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecurityConstantConfiguration {
    @Bean
    public String securityDbUrl(@Value("${security.db.url}") String securityDbUrl) {
        return securityDbUrl;
    }

    @Bean
    public String securityDbName(@Value("${security.db.name}") String securityDbName) {
        return securityDbName;
    }

    @Bean
    public String securityDbCollection(@Value("${security.db.collection}") String securityDbCollection) {
        return securityDbCollection;
    }

    @Bean
    public String tokenType(@Value("${jwt.token.type}") String tokenType) {
        return tokenType;
    }

    @Bean
    public String getPublicKeyEndpoint(@Value("${client-management.app.endpoint.for-get-public-key}") String getPublicKeyEndpoint) {
        return getPublicKeyEndpoint;
    }
}
