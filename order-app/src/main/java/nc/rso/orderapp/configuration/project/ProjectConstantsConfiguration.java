package nc.rso.orderapp.configuration.project;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectConstantsConfiguration {
    @Bean
    public String orderDbUrl(@Value("${order.db.url}") String orderDbUrl) {
        return orderDbUrl;
    }

    @Bean
    public String orderDbName(@Value("${order.db.name}") String orderDbName) {
        return orderDbName;
    }

    @Bean
    public String orderDbCollection(@Value("${order.db.collection}") String orderDbCollection) {
        return orderDbCollection;
    }
}
