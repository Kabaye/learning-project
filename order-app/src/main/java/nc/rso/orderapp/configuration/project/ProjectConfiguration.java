package nc.rso.orderapp.configuration.project;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import nc.rso.orderapp.order.entity.Order;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Configuration
@PropertySource(value = "classpath:application.properties")
public class ProjectConfiguration {
    private final String orderDbUrl;
    private final String orderDbName;
    private final String orderDbCollection;

    public ProjectConfiguration(String orderDbUrl, String orderDbName, String orderDbCollection) {
        this.orderDbUrl = orderDbUrl;
        this.orderDbName = orderDbName;
        this.orderDbCollection = orderDbCollection;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConf() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public MongoCollection<Order> orderCollection() {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        return MongoClients.create("mongodb://" + orderDbUrl).getDatabase(orderDbName).getCollection(orderDbCollection, Order.class).withCodecRegistry(pojoCodecRegistry);
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper model = new ModelMapper();
        model.getConfiguration()
                .setSkipNullEnabled(true)
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);
        return model;
    }
}
