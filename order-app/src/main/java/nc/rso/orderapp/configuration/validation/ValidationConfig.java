package nc.rso.orderapp.configuration.validation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ValidationConfig {
    private static final Integer MIN_ORDER_NAME_LENGTH = 4;
    private static final Double MIN_AMOUNT_OF_ORDER = 0.01;

    @Bean
    public Integer minNameLength() {
        return MIN_ORDER_NAME_LENGTH;
    }

    @Bean
    public Double minOrderAmount() {
        return MIN_AMOUNT_OF_ORDER;
    }
}
