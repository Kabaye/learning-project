package nc.rso.orderapp.order.validation;

public interface PropertyValidator<T> {
    void check(T property);
}
