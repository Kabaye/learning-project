package nc.rso.orderapp.order.service;

import nc.rso.orderapp.order.entity.Order;

import java.util.Collection;

public interface OrderService {
    Order save(Order order);

    Order find(String id);

    Collection<Order> findAll();

    Order update(Order order);

    void delete(String id);

    void deleteByClientUsername(String clientUsername);
}
