package nc.rso.orderapp.order.validation;

import nc.rso.orderapp.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

@Validator
public class OrderAmountValidator implements PropertyValidator<Double> {
    private final Double minOrderAmount;

    @Autowired
    public OrderAmountValidator(Double minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    @Override
    public void check(Double amount) {
        if (amount < minOrderAmount) {
            throw new ServiceException("Amount = " + amount + " is too small");
        }
    }
}
