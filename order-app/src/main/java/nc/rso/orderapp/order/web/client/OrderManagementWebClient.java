package nc.rso.orderapp.order.web.client;

import nc.rso.orderapp.order.dto.ClientDTO;
import nc.rso.orderapp.security.util.JwtTokenUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Component
public class OrderManagementWebClient {
    private final RestTemplate restTemplate;
    private final JwtTokenUtils jwtTokenUtils;
    private final String getClientByUsernameEndpoint;
    private final String clientControllerUrl;
    private final String clientManagementBaseUrl;
    private final HttpHeaders headers = new HttpHeaders();

    public OrderManagementWebClient(RestTemplate restTemplate, JwtTokenUtils jwtTokenUtils, String getClientByUsernameEndpoint, String clientControllerUrl, String clientManagementBaseUrl) {
        this.restTemplate = restTemplate;
        this.jwtTokenUtils = jwtTokenUtils;
        this.getClientByUsernameEndpoint = getClientByUsernameEndpoint;
        this.clientControllerUrl = clientControllerUrl;
        this.clientManagementBaseUrl = clientManagementBaseUrl;
    }

    public Boolean isClientAuthorized(String clientUsername, String token) {
        headers.setBearerAuth(token);
        ResponseEntity<ClientDTO> clientDTO = restTemplate.exchange(clientManagementBaseUrl + clientControllerUrl + getClientByUsernameEndpoint + "/" + clientUsername, HttpMethod.GET, new HttpEntity<>(headers), ClientDTO.class);
        var username = jwtTokenUtils.validateTokenAndGetUsername(token);
        return Objects.nonNull(clientDTO.getBody()) && clientDTO.getBody().getUsername().equals(username);
    }
}
