package nc.rso.orderapp.order.controller;

import nc.rso.orderapp.exception.AuthorizationException;
import nc.rso.orderapp.order.dto.OrderDTO;
import nc.rso.orderapp.order.service.OrderService;
import nc.rso.orderapp.order.web.client.OrderManagementWebClient;
import nc.rso.orderapp.util.OrderMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Controller
@RequestMapping(value = "/api/v1/orders")
public class OrderController {
    private final OrderService orderService;
    private final OrderMapper mapper;
    private final OrderManagementWebClient client;
    private final String tokenType;

    public OrderController(OrderService orderService, OrderMapper mapper, OrderManagementWebClient client, String tokenType) {
        this.orderService = orderService;
        this.mapper = mapper;
        this.client = client;
        this.tokenType = tokenType + " ";
    }

    @PostMapping("/save")
    public ResponseEntity<OrderDTO> save(@RequestBody OrderDTO orderDto, @RequestHeader HttpHeaders headers) {
        String token = Objects.requireNonNull(headers.getFirst(AUTHORIZATION)).substring(tokenType.length());
        if (!client.isClientAuthorized(orderDto.getClientUsername(), token)) {
            throw new AuthorizationException("Client with email = " + orderDto.getClientUsername() + " is not authorized or token is not valid. Please, authorize before purchase or use valid token.");
        }
        return new ResponseEntity<>(mapper.toDto(orderService.save(mapper.toEntity(orderDto))), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<OrderDTO>> findAll() {
        return new ResponseEntity<>(orderService.findAll().stream()
                .map(mapper::toDto)
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDTO> find(@PathVariable String id) {
        return new ResponseEntity<>(mapper.toDto(orderService.find(id)), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<OrderDTO> update(@PathVariable String id, @RequestBody OrderDTO orderDto, @RequestHeader HttpHeaders headers) {
        String token = Objects.requireNonNull(headers.getFirst(AUTHORIZATION)).substring(tokenType.length());
        if (!client.isClientAuthorized(orderDto.getClientUsername(), token)) {
            throw new AuthorizationException("Client with email = " + orderDto.getClientUsername() + " is not exist or token is not valid. Please, authorize with existent client before update or use valid token.");
        }
        orderDto.setId(id);
        return new ResponseEntity<>(mapper.toDto(orderService.update(mapper.toEntity(orderDto))), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable String id) {
        orderService.delete(id);
        return new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteOrdersByClientUsername(@RequestParam(name = "client-username") String clientUsername) {
        orderService.deleteByClientUsername(clientUsername);
        return new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    }

}
