package nc.rso.orderapp.order.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Client {
    private String username;
    private String email;
    private String password;
}
