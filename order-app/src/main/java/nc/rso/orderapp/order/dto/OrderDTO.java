package nc.rso.orderapp.order.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OrderDTO {
    private String id;
    private String clientUsername;
    private String name;
    private String producer;
    private String producerCountry;
    private Double amount;
    private String amountType;
}
