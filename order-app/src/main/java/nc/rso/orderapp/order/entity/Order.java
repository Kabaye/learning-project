package nc.rso.orderapp.order.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.util.StringJoiner;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    private ObjectId id;
    private String clientUsername;
    private String name;
    private String producer;
    private String producerCountry;
    private Double amount;
    private String amountType;

    @Override
    public String toString() {
        return new StringJoiner(", ", Order.class.getSimpleName() + " [", "];")
                .add("id = '" + id + "'")
                .add("name = '" + name + "'")
                .add("clientUsername= '" + clientUsername + "'")
                .add("producer = '" + producer + "'")
                .add("producerCountry = '" + producerCountry + "'")
                .add("amount = " + amount + " " + amountType)
                .toString();
    }
}
