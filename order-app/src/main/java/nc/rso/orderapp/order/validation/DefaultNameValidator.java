package nc.rso.orderapp.order.validation;

import nc.rso.orderapp.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

@Validator
public class DefaultNameValidator implements PropertyValidator<String> {
    private final Integer minNameLength;

    @Autowired
    public DefaultNameValidator(Integer minNameLength) {
        this.minNameLength = minNameLength;
    }

    @Override
    public void check(String name) {
        if (name.length() < minNameLength) {
            throw new ServiceException("Name \"" + name + "\" is too short.");
        }

    }
}
