package nc.rso.orderapp.order.service;

import nc.rso.orderapp.exception.EntityNotFoundException;
import nc.rso.orderapp.order.dao.OrderDAO;
import nc.rso.orderapp.order.entity.Order;
import nc.rso.orderapp.order.validation.ServiceValidator;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Objects;

@Service
public class DefaultOrderService implements OrderService {
    private final OrderDAO orderDAO;
    private final ServiceValidator<Order, ObjectId> serviceValidator;

    @Autowired
    public DefaultOrderService(OrderDAO orderDAO, ServiceValidator<Order, ObjectId> serviceValidator) {
        this.orderDAO = orderDAO;
        this.serviceValidator = serviceValidator;
    }

    @Override
    public Order save(Order order) {
        serviceValidator.checkForSave(order);
        order.setClientUsername(order.getClientUsername().toLowerCase());
        return orderDAO.save(order);
    }

    @Override
    public Order find(String id) {
        ObjectId objectId = new ObjectId(id);
        serviceValidator.checkIdIsNotNull(objectId);
        Order order = orderDAO.find(objectId);
        if (Objects.isNull(order)) {
            throw new EntityNotFoundException("Order with id: \'" + id + "\' doesn't exist.");
        }
        return order;
    }

    @Override
    public Collection<Order> findAll() {
        return orderDAO.findAll();
    }

    @Override
    public Order update(Order order) {
        serviceValidator.checkForUpdate(order);
        order.setClientUsername(order.getClientUsername().toLowerCase());
        return orderDAO.update(order);
    }

    @Override
    public void delete(String id) {
        ObjectId objectId = new ObjectId(id);
        serviceValidator.checkIdIsNotNull(objectId);
        orderDAO.delete(objectId);
    }

    @Override
    public void deleteByClientUsername(String clientUsername) {
        orderDAO.deleteByClientUsername(clientUsername);
    }
}
