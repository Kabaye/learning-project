package nc.rso.orderapp.order.dao;

import nc.rso.orderapp.order.entity.Order;
import org.bson.types.ObjectId;

import java.util.Collection;

public interface OrderDAO {
    Order save(Order order);

    Order find(ObjectId id);

    Collection<Order> findAll();

    Order update(Order order);

    void delete(ObjectId id);

    void deleteByClientUsername(String clientUsername);
}
