package nc.rso.orderapp.order.validation;

import nc.rso.orderapp.exception.ServiceException;
import nc.rso.orderapp.order.entity.Order;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

@Validator
public class OrderValidator implements ServiceValidator<Order, ObjectId> {
    private final PropertyValidator<String> nameValidator;
    private final PropertyValidator<Double> amountValidator;

    @Autowired
    public OrderValidator(PropertyValidator<String> nameValidator, PropertyValidator<Double> amountValidator) {
        this.nameValidator = nameValidator;
        this.amountValidator = amountValidator;
    }

    @Override
    public ObjectId extractId(Order resource) {
        return resource.getId();
    }

    @Override
    public void checkIdIsNull(ObjectId id) {
        if (Objects.nonNull(id)) {
            throw new ServiceException("ID of order must be null.");
        }
    }

    @Override
    public void checkIdIsNotNull(ObjectId id) {
        if (Objects.isNull(id)) {
            throw new ServiceException("ID of order must be not null.");
        }
    }

    @Override
    public void checkNotNull(Order resource) {
        if (Objects.isNull(resource)) {
            throw new ServiceException("Order can't be null.");
        }
    }

    @Override
    public void checkNotNull(Order resource, String errorMessage) {
        if (Objects.isNull(resource)) {
            throw new ServiceException(errorMessage);
        }
    }

    @Override
    public void checkProperties(Order resource) {
        nameValidator.check(resource.getName());
        amountValidator.check(resource.getAmount());
    }
}
