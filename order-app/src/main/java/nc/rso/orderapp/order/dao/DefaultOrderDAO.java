package nc.rso.orderapp.order.dao;

import com.mongodb.client.MongoCollection;
import nc.rso.orderapp.order.entity.Order;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

@Repository
public class DefaultOrderDAO implements OrderDAO {
    private static final String ID_ATTRIBUTE_NAME = "_id";
    private static final String CLIENT_USERNAME_ATTRIBUTE_NAME = "clientUsername";
    private final MongoCollection<Order> orderCollection;

    public DefaultOrderDAO(MongoCollection<Order> orderCollection) {
        this.orderCollection = orderCollection;
    }

    @Override
    public Order save(Order order) {
        orderCollection.insertOne(order);
        return order;
    }

    @Override
    public Order find(ObjectId id) {
        return orderCollection.find(eq(ID_ATTRIBUTE_NAME, id)).first();
    }


    @Override
    public Collection<Order> findAll() {
        Collection<Order> orders = new ArrayList<>();
        for (var order :
                orderCollection.find()) {
            orders.add(order);
        }
        return orders;
    }

    @Override
    public Order update(Order order) {
        Order dbOrder = find(order.getId());

        String clientUsername = Objects.isNull(order.getClientUsername()) ? dbOrder.getClientUsername() : order.getClientUsername();

        orderCollection.updateOne(eq(ID_ATTRIBUTE_NAME, order.getId()),
                combine(set("clientUsername", clientUsername), set("name", order.getName()), set("producer", order.getProducer()),
                        set("producerCountry", order.getProducerCountry()), set("amount", order.getAmount()), set("amountType", order.getAmountType())));

        return find(order.getId());
    }

    @Override
    public void delete(ObjectId id) {
        orderCollection.deleteOne(eq(ID_ATTRIBUTE_NAME, id));
    }

    @Override
    public void deleteByClientUsername(String clientUsername) {
        orderCollection.deleteMany(eq(CLIENT_USERNAME_ATTRIBUTE_NAME, clientUsername));
    }
}
