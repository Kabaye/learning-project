package nc.rso.orderapp.exception;

import org.springframework.http.HttpStatus;

public class RemoteAPIException extends OrderAppException {
    public RemoteAPIException(String message, HttpStatus status) {
        super("Exception from remote API: \"" + message + "\";", status);
    }
}