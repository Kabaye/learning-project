package nc.rso.orderapp.exception;

public class AuthorizationException extends SecurityException {
    public AuthorizationException(String message) {
        super(message);
    }
}
