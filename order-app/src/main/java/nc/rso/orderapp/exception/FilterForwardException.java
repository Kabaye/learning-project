package nc.rso.orderapp.exception;

import org.springframework.http.HttpStatus;

public class FilterForwardException extends OrderAppException {
    public FilterForwardException(String message, HttpStatus status) {
        super(message, status);
    }
}
