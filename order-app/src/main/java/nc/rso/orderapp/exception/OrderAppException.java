package nc.rso.orderapp.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class OrderAppException extends RuntimeException {
    private HttpStatus status;

    OrderAppException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }
}
