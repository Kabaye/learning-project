package nc.rso.orderapp.exception;

import org.springframework.http.HttpStatus;

public class SecurityException extends OrderAppException {
    public SecurityException(String message) {
        super(message, HttpStatus.UNAUTHORIZED);
    }
}
