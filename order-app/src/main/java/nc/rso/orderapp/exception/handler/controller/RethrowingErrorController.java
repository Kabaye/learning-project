package nc.rso.orderapp.exception.handler.controller;

import nc.rso.orderapp.exception.FilterForwardException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@RestController
public class RethrowingErrorController {

    private static final String DEFAULT_ERROR_MESSAGE = "Uh-oh... A not-so fatal error has occurred on %s.\n" +
            "Sadly the page or resource you are looking for cannot be found - but fortunately you can see all endpoints, " +
            "if you will go on /swagger-ui.html.\n" + "     ***     \n" + "You can't press any key to terminate this message and be re-directed somewhere.\n" +
            "But you can press CTRL+ALT+DEL if you want, it won't have any affect on my server but will bring up Task Manager or you can Lock your computer " +
            "and go for lunch, its really up to you where you go from here. \n" + "Have a Good Day, Bye!";

    //TODO: find a way to store this into configs
    private static final String ERROR_MAPPING = "/404";

    /**
     * Извлекает из реквеста случивлуюся ошибку и пробрасывает, чтобы о ней позаботились
     *
     * @param request request, that caused an exception somewhere in filters
     * @link GlobalExceptionHandler (<code>@ControllerAdvice</code>)
     */
    @RequestMapping(ERROR_MAPPING) // принимает любой http method
    public void getError(HttpServletRequest request) {

        // rethrow any exception, that happened in filters to @ControllerAdvice
        Object val = request.getAttribute("javax.servlet.error.exception");
        if (val instanceof RuntimeException) {
            throw (RuntimeException) val;
        }

        Object errorMessage = request.getAttribute("javax.servlet.error.message");
        if (errorMessage instanceof String) {
            if (errorMessage.equals("Access Denied")) {
                throw new FilterForwardException((String) errorMessage, HttpStatus.FORBIDDEN);
            }
        }

        /*
         or request uri might be wrong, thus not found.
         if so, do the same stuff that org.apache.catalina.core.StandardWrapperValve does
         before redirecting request with exceptions to /error
        */

        throw new FilterForwardException(String.format(DEFAULT_ERROR_MESSAGE, ZonedDateTime.now().format(DateTimeFormatter.ISO_ZONED_DATE_TIME)), HttpStatus.NOT_FOUND);
    }
}