package nc.rso.orderapp.exception;

import org.springframework.http.HttpStatus;

public class ServiceException extends OrderAppException {
    public ServiceException(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
