package nc.rso.orderapp.exception;

import org.springframework.http.HttpStatus;

public class UnknownStateException extends OrderAppException {
    public UnknownStateException(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
